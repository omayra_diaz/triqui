
var turno = "X";
var totalJugadas = 0;

function iniciarJuego() {
  let i;

  let divTablero = document.getElementById("divTablero"); // en esta linea se declara que en la seccion del tablero se cree un docuemnto para ejecutar el codigo, algo asi como mostrar una ventana.
  
  for (i = 1; i < 10; i++) {
    let unaCasilla = /* asi se llamam la variable que va a tomar valores y se tra baja mas adelante*/ document.createElement("input"); // se crea un elemento para ingresar valores o que tome parametros por eso se llama "input".
    unaCasilla.type = "button";// se crea la casilla con la caracteristica de que es un boton.
    unaCasilla.value = " "; // la casilla va a tener un valor vacio que mas adelante se le va asignar.
    unaCasilla.setAttribute("id", "casilla" + i); // de la linea 13 a 15 se utiliza para actualizar los valores de las casillas cada vez que se a hace click, 
    unaCasilla.setAttribute("class", "casilla");
    unaCasilla.setAttribute("onClick", "realizarJugada(this.id)");
    divTablero.appendChild(unaCasilla);// en esta parte se crea para repetir el proceso de lo scuadros del tablero cada tercer cuadro haga un salto y empiece otra vez.
    if (i % 3 === 0) {
      let salto = document.createElement("br");
      divTablero.appendChild(salto);
    }
  }
}


//para calcular si gano o empato
function realizarJugada(idCasilla) {
  let resultado, triqui;
  resultado = marcarCasilla(idCasilla); // se crea la funcion para empezar a jugar, la variable resultado se le asignan los valoeres de la casilla que se seleccione.

  if (resultado == true) {
    totalJugadas++;
    triqui = calcularTriqui();// se le asigna valor a la variable triqui de acuerdo al valor que arroje la funcion "calcularTriqui()"
    if (triqui) {
      alert("Has ganado " + turno + " !!!!");
      borrarTablero();// una vez se gane se limpia el tablero para empezar otro juego.
    } else if (totalJugadas == 9) {
      alert("Ocurrió un empate!!!");
      borrarTablero();
    } else {
      cambiarTurno();
    }
  }// la s condicionales son para ir verificando casilla por casilla mientras acumula valores y luego los compara para determinar si hay un empate o un ganador, las compara dependiendo del valor que arroje el llamado a la funcion "calcular triqui".

  return false;
}

//para cambiar el turno
function cambiarTurno() {
  if (turno === "X") {
    turno = "O";
  } else {
    turno = "X";
  }
}//las condicionales en esta funcion son para que una vez se marque X en una casilla la proxima vez se marque O

//calcular si la casilla esta ocupada

function marcarCasilla(idCasilla) {
  let casilla = document.getElementById(idCasilla);// se crea un elemento para poder marcar casiilas con X u O

  if (casilla.value === "X" || casilla.value === "O") {
    alert("Casilla ocupada. Por favor seleccione otra.");// esta condicional indica que si hay una X o una O salga una alerta con casilla ocupada.
    return false;
  }
  casilla.value = turno;

  return true;
}

//calcular el triki

function calcularTriqui() {
  //Calcular triqui en filas
  //declarar 3 variables
  let i;
  let casilla1;
  let casilla2;
  let casilla3;
  // se declaran las variables para comparar las casillas y determibnar si hay triqui 
 //filas

  for (i = 0; i < 9; i = i + 3) {
    casilla1 = document.getElementById("casilla" + (i + 1)).value;//primera casilla primera casilla con valor 1
    casilla2 = document.getElementById("casilla" + (i + 2)).value;//segunda casilla (primera casilla+2)
    casilla3 = document.getElementById("casilla" + (i + 3)).value;//tercera casilla con (primera casilla + 3)
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  } // en este ciclo for se compara casilla por casilla en este caso las tres horizontales y determinar si hay triqui, de tal manera que si las tres casillas tienen el mismo valor se gana.

  //Calcular triqui en columnas
  for (i = 1; i < 4; i++) {
    casilla1 = document.getElementById("casilla" + i).value;//primera casilla con valor de 1
    casilla2 = document.getElementById("casilla" + (i + 3)).value;//siguiente casilla sumandole 3 a la primera
    casilla3 = document.getElementById("casilla" + (i + 6)).value;// tercera casilla a comparar con valro de la primera + 6.
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }//en este ciclo for compara las casillas verticales y determina si hay tres valores repetidos en cada hilera vertical de casillas

  //Calcular triqui en diagonales

  let valor = [2, 4]; // valor No 1=2, el valor No 2 =4
  for (i = 0; i < 2; i++) {
    casilla1 = document.getElementById("casilla" + 5).value;//parte de la casilla 5 o la casilla del centro 
    casilla2 = document.getElementById("casilla" + (5 + valor[i])).value;// compara la casilla 5 + el valor que corresponde a i puede ser (2 o 4) arrojando las casillas 7 o 9
    casilla3 = document.getElementById("casilla" + (5 - valor[i])).value;// compara la casilla 5 - el valor que corresponde a i puede ser (2 o 4) arrojando las casillas 3 o 1
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }// determina si hay triqui en las casiillas diagonales 

  return false;
}

function borrarTablero() {
  totalJugadas = 0;
  turno = "X";
  for (i = 1; i < 10; i++) {
    casilla = document.getElementById("casilla" + i); //determina ekl valor de la casilla actual y lo cambia al valor requerido.
    casilla.value = " "; //le asigna un valor vacio a cada casilla
  }
} // funcion para borra el tablero asignando un valor vacio a cada casilla,
